import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";

import { AppComponent } from './app.component';
import { AppRoutingModule } from "./app-routing.module";
import { ImageComponent } from "./components/image/image.component";
import { CreateImageComponent } from "./pages/create-image/create-image.component";
import { ImageListComponent } from "./pages/image-list/image-list.component";
import { ImageDetailComponent } from "./pages/image-detail/image-detail.component";
import { RegisterComponent } from "./pages/register/register.component";
import { LoginComponent } from "./pages/login/login.component";
import { AuthInterceptor } from "./interceptors/auth.interceptor";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { LayoutComponent } from "./layouts/layouts/layout.component";
import { MatFormFieldModule } from "@angular/material/form-field";

@NgModule({
  declarations: [
    AppComponent,
    ImageComponent,
    CreateImageComponent,
    ImageListComponent,
    ImageDetailComponent,
    RegisterComponent,
    LoginComponent,
    AuthLayoutComponent,
    LayoutComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
