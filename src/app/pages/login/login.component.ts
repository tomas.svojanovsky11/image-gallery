import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, NonNullableFormBuilder, Validators } from "@angular/forms";
import { LoginRequest } from "../../services/types";
import { catchError, of, take, throwError } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { LoginService } from "../../services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ["login.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  form = this.formBuilder.group({
    email: ["", [Validators.required]],
    password: ["", Validators.required],
  });

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private loginService: LoginService,
    private snackBar: MatSnackBar,
    private router: Router
    ) {
  }

  get email() {
    return this.form.get("email") as FormControl
  }

  get password() {
    return this.form.get("password") as FormControl;
  }

  onSubmit() {
    const data = this.form.value as LoginRequest;

    this.loginService.login(data)
      .pipe(
        take(1),
        catchError((err) => {
          this.snackBar.open("Something went wrong", "Close");
          return throwError(err);
        }),
      )
      .subscribe((response) => {
        this.snackBar.open("You were logged in", "Close");
        this.router.navigateByUrl(`/user/${response.user.id}/image-list`);
      });
  }
}
