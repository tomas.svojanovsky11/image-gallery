import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-image-detail',
  templateUrl: 'image-detail.component.html',
  styleUrls: ["image-detail.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageDetailComponent {

}
