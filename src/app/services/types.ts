export type User = {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
};

export type LoginResponse = {
  user: User;
  accessToken: string;
};

export type LoginRequest = {
  email: string;
  password: string;
};

export type RegisterRequest = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export type ImageResponse = {
  id: number;
  imageUrl: string;
  name: string;
  likes: number
  createdAt: string;
};

export type ImageRequest = {
  imageUrl: string;
  name: string;
};

export type ImageUpdateRequest = {
  likes: number;
} & ImageRequest;
