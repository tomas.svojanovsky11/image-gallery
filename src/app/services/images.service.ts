import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { ImageRequest, ImageResponse, ImageUpdateRequest } from "./types";

@Injectable({
  providedIn: "root",
})
export class ImagesService {

  constructor(private http: HttpClient) {
  }

  getByUser(id: number) {
    return this.http.get<ImageResponse[]>(`http://localhost:3000/api/v1/users/${id}/images`);
  }

  get(id: number) {
    return this.http.get<ImageResponse>(`http://localhost:3000/api/v1/image/${id}`);
  }

  create(userId: number, request: ImageRequest) {
    return this.http.post<ImageResponse>(`http://localhost:3000/api/v1/users/${userId}/image`, request);
  }

  update(id: number, request: Partial<ImageUpdateRequest>) {
    return this.http.patch<ImageResponse>(`http://localhost:3000/api/v1/image/${id}`, request);
  }

  delete(id: number) {
    return this.http.delete<void>(`http://localhost:3000/api/v1/image/${id}`);
  }
}
