import { Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CreateImageComponent } from "./pages/create-image/create-image.component";
import { ImageListComponent } from "./pages/image-list/image-list.component";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { LayoutComponent } from "./layouts/layouts/layout.component";
import { ApplicationGuard } from "./guards/application.guard";
import { AuthGuard } from "./guards/auth.guard";

const routes: Route[] = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "login",
  },
  {
    path: "user/:id",
    component: AuthLayoutComponent,
    canActivate: [ApplicationGuard],
    children: [
      {
        component: ImageListComponent,
        path: "image-list",
      },
      {
        component: CreateImageComponent,
        path: "create-image",
      },
    ],
  },
  {
    path: "",
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "login",
        component: LoginComponent,
      },
      {
        path: "register",
        component: RegisterComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
