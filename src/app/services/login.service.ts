import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { tap } from "rxjs";

import { LoginRequest, LoginResponse } from "./types";
import { JwtTokenService } from "./jwt-token.service";

@Injectable({ providedIn: "root" })
export class LoginService {

  constructor(private http: HttpClient, private jwtToken: JwtTokenService) {
  }

  login(request: LoginRequest) {
    return this.http.post<LoginResponse>("http://localhost:3000/api/v1/auth", request)
      .pipe(tap(response => {
          this.jwtToken.set(response.accessToken);
      }))
  }
}
