import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormControl, NonNullableFormBuilder, Validators } from "@angular/forms";
import { take } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";

import { ImagesService } from "../../services/images.service";
import { ImageRequest } from "../../services/types";

@Component({
  selector: 'app-create-image',
  templateUrl: 'create-image.component.html',
  styleUrls: ["create-image.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateImageComponent {
  userId = this.route.parent?.snapshot.paramMap.get("id");

  form = this.formBuilder.group({
    imageUrl: ["", [Validators.required]],
    name: ["", [Validators.required]],
  });

  constructor(
    private route: ActivatedRoute,
    private formBuilder: NonNullableFormBuilder,
    private imageService: ImagesService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {
  }

  get imageUrl() {
    return this.form.get("imageUrl") as FormControl;
  }

  get name() {
    return this.form.get("name") as FormControl;
  }

  onSubmit() {
    const data = this.form.value as ImageRequest;

    this.imageService.create(Number(this.userId), data)
      .pipe(take(1))
      .subscribe(() => {
        this.snackBar.open("The image was created", "Close");
        this.router.navigate(["user", this.userId, "image-list"]);
      });
  }
}
