import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { switchMap, take } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

import { ImagesService } from "../../services/images.service";
import { ImageResponse } from "../../services/types";

@Component({
  selector: 'app-image-list',
  templateUrl: 'image-list.component.html',
  styleUrls: ["image-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageListComponent implements OnInit {

  userId = this.route.parent?.snapshot.paramMap.get("id") as string;

  images: ImageResponse[] = [];

  images$ = this.imagesService.getByUser(Number(this.userId))
    .pipe(take(1));

  constructor(
    private imagesService: ImagesService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit(): void {
    this.images$
      .subscribe(this.refreshImages.bind(this));
  }

  onDelete(id: number) {
    this.imagesService.delete(id)
      .pipe(
        take(1),
        switchMap(() => {
          return this.images$;
        }),
      )
      .subscribe((images) => {
        this.snackBar.open("Image was deleted", "Close");
        this.refreshImages(images);
      });
  }

  onLike({ id, likes }: { id: number, likes: number }) {
    this.imagesService.update(id, {
      likes,
    }).pipe(
      take(1),
      switchMap(() => this.images$)
    )
      .subscribe((images) => {
          this.refreshImages(images);
      });
  }

  refreshImages(images: ImageResponse[]) {
    this.images = images;
    this.cdr.markForCheck();
  }
}
