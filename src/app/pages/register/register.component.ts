import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, NonNullableFormBuilder, Validators } from "@angular/forms";

import { RegisterService } from "../../services/register.service";
import { catchError, of, take } from "rxjs";
import { RegisterRequest } from "../../services/types";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ["register.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent {
  form = this.formBuilder.group({
    email: ["", [Validators.required, Validators.email]],
    firstName: ["", [Validators.required]],
    lastName: ["", Validators.required],
    password: ["", Validators.required],
  });

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private registerService: RegisterService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  get email() {
    return this.form.get("email") as FormControl
  }

  get firstName() {
    return this.form.get("firstName") as FormControl;
  }

  get lastName() {
    return this.form.get("lastName") as FormControl;
  }

  get password() {
    return this.form.get("password") as FormControl;
  }

  onSubmit() {
    const data = this.form.value as RegisterRequest;

    this.registerService.register(data)
      .pipe(
        take(1),
        catchError(() => {
          this.snackBar.open("Something went wrong", "Close");
          return of("Something went wrong");
        }),
      )
      .subscribe(() => {
        this.snackBar.open("You were successfully registered", "Close");
        this.router.navigateByUrl("/login");
      });
  }
}
