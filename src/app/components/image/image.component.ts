import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { ImageResponse } from "../../services/types";

@Component({
  selector: 'app-image',
  templateUrl: 'image.component.html',
  styleUrls: ['image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageComponent {
  @Input() image!: ImageResponse;

  @Output() outLike = new EventEmitter<{ id: number, likes: number }>();

  @Output() outDelete = new EventEmitter<number>();

  onLike(id: number) {
    this.outLike.emit({ id, likes: this.image.likes + 1 });
  }

  onDelete(id: number) {
    this.outDelete.emit(id);
  }
}
