import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { RegisterRequest } from "./types";

@Injectable({ providedIn: "root" })
export class RegisterService {

  constructor(private http: HttpClient) {
  }

  register(request: RegisterRequest) {
    return this.http.post("http://localhost:3000/api/v1/user", request);
  }
}
