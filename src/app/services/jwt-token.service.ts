import { Injectable } from '@angular/core';

const JWT_TOKEN_KEY = "jwtTokenKey";

@Injectable({ providedIn: "root" })
export class JwtTokenService {
  set(token: string) {
    localStorage.setItem(JWT_TOKEN_KEY, token);
  }

  get() {
    const token = localStorage.getItem(JWT_TOKEN_KEY);
    return token || undefined;
  }

  clear() {
    localStorage.removeItem(JWT_TOKEN_KEY);
  }
}
